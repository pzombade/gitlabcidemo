package com.mangroo.temperature.integration;

import com.mangroo.temperature.TemperatureConfiguration;
import io.cucumber.java.Before;
import io.cucumber.spring.CucumberContextConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ContextConfiguration;

@Slf4j
@CucumberContextConfiguration
@ContextConfiguration
@SpringBootTest(classes = TemperatureConfiguration.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TemperatureConfigurationIT {

    @LocalServerPort
    int randomServerPort;

    @Before
    public void setUp() {
        log.info("TemperatureConfigurationIT randomServerPort {}", randomServerPort);
        log.info("-------------- Spring Context Initialized For Executing Cucumber Tests --------------");
    }

    @Test
    public void temperatureConfigurationLoads() {
        log.info("temperatureConfigurationLoads");
    }

}
